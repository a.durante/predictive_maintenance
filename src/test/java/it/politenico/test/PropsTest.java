package it.politenico.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.BeforeClass;
import org.junit.Test;

import it.politecnico.coolprop.CoolProp;
import it.politecnico.utils.ConverterUtils;

public class PropsTest {
	
	@BeforeClass
	public static void init() {
		System.loadLibrary("CoolProp");
	}
	
	@Test
	public void evaporatoreIngresso() {
		try {
			double entalpiaCondensOut = ConverterUtils.kiloToUnit(239.95d);
			double kelvinEvpIn = ConverterUtils.celsiusToKelvin(6.45144d);
			double pascalEvpIn = ConverterUtils.barToPascal(3.60796d);
			
			String phaseInEvap = CoolProp.PhaseSI("H", entalpiaCondensOut, "P", pascalEvpIn, "R134a");
			assertTrue("twophase".equals(phaseInEvap));
			
			double q = CoolProp.PropsSI("Q", "H", entalpiaCondensOut, "P", pascalEvpIn, "R134a");
			assertTrue(q <1);
			
			double tsatEvp = CoolProp.PropsSI("T", "Q", 0, "P", pascalEvpIn, "R134a");
			assertTrue(tsatEvp != kelvinEvpIn);
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	@Test
	public void evaporatoreUscita() {
		try {
			
			double kelvinEvpOut = ConverterUtils.celsiusToKelvin(6.70037d);
			double pascalEvpOut = ConverterUtils.barToPascal(3.09609d);
			
			String phaseInEvap = CoolProp.PhaseSI("T", kelvinEvpOut, "P", pascalEvpOut, "R134a");
			assertTrue("gas".equals(phaseInEvap));
			
			double tsatEvpOut = CoolProp.PropsSI("T", "Q", 1, "P", pascalEvpOut, "R134a");
			assertTrue(kelvinEvpOut > tsatEvpOut);
			System.out.println(kelvinEvpOut-tsatEvpOut);
			
			double tsatEvpIn = CoolProp.PropsSI("T", "Q", 0, "P", pascalEvpOut, "R134a");
			assertTrue(tsatEvpIn == tsatEvpOut);
			
			double hsatEvpOut = CoolProp.PropsSI("H", "Q", 1, "P", pascalEvpOut, "R134a");
			double hsatReal = CoolProp.PropsSI("H", "T", kelvinEvpOut, "P", pascalEvpOut, "R134a");
			assertTrue(hsatReal > hsatEvpOut);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	@Test
	public void condensatoreUscita() {
		double kelvinEvpOut = ConverterUtils.celsiusToKelvin(30.2645d);
		double pascalEvpOut = ConverterUtils.barToPascal(7.589d);
		String phaseInEvap = CoolProp.PhaseSI("T", kelvinEvpOut, "P", pascalEvpOut, "R134a");
		System.out.println(phaseInEvap);
	}

}
