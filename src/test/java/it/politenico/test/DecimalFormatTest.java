package it.politenico.test;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;

public class DecimalFormatTest {
	
	@Test
	public void doTest() throws ParseException {
		String n = "2,005.27657";
		DecimalFormatSymbols symbol = new DecimalFormatSymbols(Locale.ENGLISH);
		DecimalFormat format = new DecimalFormat("##,##,##,###.##", symbol);
		Number parse = format.parse(n);
		System.out.println(parse.doubleValue());
		Assert.assertTrue(parse.doubleValue() > 2000d);
		System.out.println(parse.doubleValue()-2005d);
		Assert.assertTrue(parse.doubleValue()-2005d > 0d);
	}

}
