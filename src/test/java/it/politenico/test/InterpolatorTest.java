package it.politenico.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import it.politecnico.utils.RefrPropertiesInterpolator;

public class InterpolatorTest {
	
	@Test
	public void computePropertiesForR134a() {
		
		try {
			String refrId = "R134a";
			Double t1 = RefrPropertiesInterpolator.getTemperatureByEnthalphyAndPressure(refrId, 244.54d, 4d);
			assertTrue(t1.doubleValue() == 8.93d);
			
			Double t2 = RefrPropertiesInterpolator.getTemperatureByEnthalphyAndPressure(refrId, 244.54d, 4.5d);
			assertTrue(t2.doubleValue() == 12.48d);
			
			Double t3 = RefrPropertiesInterpolator.getTemperatureByEnthalphyAndPressure(refrId, 240.17d, 3.7620d);
			assertTrue(t3.doubleValue() == 7.12d);
			
			Double h1 = RefrPropertiesInterpolator.getEnthalpyByTemperatureAndPressure(refrId, 15.416d, 4.0965d);
			assertTrue(h1.doubleValue() == 409.53d);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void computeSatPropertiesForR134a() {
		try {
			String refrId = "R134a";
			
			Double t1 = RefrPropertiesInterpolator.getLiquidSatTempByPressure(refrId, 1.5d);
			assertTrue(t1.doubleValue() == -17.13d);
			
			Double t2 = RefrPropertiesInterpolator.getLiquidSatTempByPressure(refrId, 2.8d);
			assertTrue(t2.doubleValue() == -1.23d);
			
			Double t3 = RefrPropertiesInterpolator.getVapSatTempByPressure(refrId, 23.6d);
			assertTrue(t3.doubleValue() == 74.92d);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
