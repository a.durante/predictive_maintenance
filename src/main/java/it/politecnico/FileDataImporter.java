package it.politecnico;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.Iterator;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import it.politecnico.mapper.measurement.MeasurementMapper;
import it.politecnico.model.MeasurementDataImport;
import it.politecnico.model.measurement.Measurement;
import it.politecnico.model.measurement.MeasurementExample;
import it.politecnico.utils.MapperHelper;
import it.politecnico.utils.RefrPropertiesInterpolator;

public class FileDataImporter {

	private static Logger LOG = Logger.getLogger(FileDataImporter.class);
	
	public static void main(String[] args) {
		new FileDataImporter().readWithOpenCSV(args[0]);
	}

	public void readWithOpenCSV(String sourceFilesFolder) {
		SqlSession session = null;
		String fileName = null;
		try {
			int fileElaborated =0;
			String refrId = "R134a";
			Integer machineId= 1;
			File folder = new File(sourceFilesFolder);
			File[] listOfFiles = folder.listFiles();
			
			if(listOfFiles.length>0) {
				
				session = MapperHelper.getMapper().openSession();
				MeasurementMapper mapper = session.getMapper(MeasurementMapper.class);

				for (File file : listOfFiles) {
					if (file.isFile()) {
						fileName = file.getName();
						
						MeasurementExample example = new MeasurementExample();
						example.createCriteria().andFileNameEqualTo(fileName);
						int fileAlreadyElaborated = mapper.countByExample(example);
						if(fileAlreadyElaborated==0) {
							Reader reader = new FileReader(file);
							
							LOG.info(String.format("Processing file %s", fileName));
							
							CsvToBean<MeasurementDataImport> csvToBean = new CsvToBeanBuilder<MeasurementDataImport>(reader)
									.withType(MeasurementDataImport.class)
									.withSeparator('\t')
									.build();
							
							Iterator<MeasurementDataImport> it = csvToBean.iterator();
							int i=0;
							while (it.hasNext()) {
								MeasurementDataImport rec = it.next();
								if(validateRecord(rec, fileName, i)) {
									Measurement measure = rec.createMeasurementRecord();
									measure.setFileName(fileName);
									
									//CONDENSER
									Double rCndHIn = RefrPropertiesInterpolator.getEnthalpyByTemperatureAndPressure(refrId,rec.getRCndTIn(), rec.getRCndPIn());
									measure.setrCndHIn(rCndHIn);
									Double rCndTsatIn = RefrPropertiesInterpolator.getVapSatTempByPressure(refrId, rec.getRCndPIn());
									measure.setrCndTsatIn(rCndTsatIn);
									
									Double rCndHOut = RefrPropertiesInterpolator.getEnthalpyByTemperatureAndPressure(refrId,rec.getRCndTOut(), rec.getRCndPOut());
									measure.setrCndHOut(rCndHOut);
									Double rCndTsatOut = RefrPropertiesInterpolator.getLiquidSatTempByPressure(refrId, rec.getRCndPOut());
									measure.setrCndTsatOut(rCndTsatOut);
									
									//EVAPORATOR
									Double rEvpHIn = RefrPropertiesInterpolator.getEnthalpyByTemperatureAndPressure(refrId,rec.getREvpTIn(), rec.getREvpPIn());
									measure.setrEvpHIn(rEvpHIn); //must be set as iso-enthalpic trasformation
									Double rEvpTsatIn = RefrPropertiesInterpolator.getLiquidSatTempByPressure(refrId, rec.getREvpPIn());
									measure.setrEvpTsatIn(rEvpTsatIn);
									
									Double rEvpHOut = RefrPropertiesInterpolator.getEnthalpyByTemperatureAndPressure(refrId,rec.getREvpTOut(), rec.getREvpPOut());
									measure.setrEvpHOut(rEvpHOut);
									Double rEvpTsatOut = RefrPropertiesInterpolator.getLiquidSatTempByPressure(refrId, rec.getREvpPOut());
									measure.setrEvpTsatOut(rEvpTsatOut);
									
									measure.setMachineId(machineId);
									
									mapper.insert(measure);
								}
								i++;
							}
							session.commit();
						}else {
							LOG.info(String.format("File %s already elaborated. I'll discard this", fileName));
						}
						fileElaborated++;
						
					} else {
						LOG.warn(String.format("File %s is not a file and will not be elaborated", file.getAbsoluteFile()));
					}
				}
			}
			
			LOG.info(String.format("File process successfully ended. %d files elaborated", fileElaborated));

		} catch (Exception e) {
			LOG.error(e,e);
			LOG.error(String.format("There was an errore while processing file %s", fileName));
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	private boolean validateRecord(MeasurementDataImport rec, String fileName, int i) {
		boolean validRecord = true;
		if(rec.getRCndTIn() == null || rec.getRCndPIn() == null){
			LOG.error(String.format("Record #%d in file %s doesn't contains any information about R_CND_T_IN or R_CND_P_IN. It will be discarded", i, fileName));
			validRecord=false;
		}
	
		if(rec.getRCndTOut() == null || rec.getRCndPOut() == null) {
			LOG.error(String.format("Record #%d in file %s doesn't contains any information about R_CND_T_OUT or R_CND_P_OUT. It will be discarded", i, fileName));			
			validRecord=false;
		}
		
		if(rec.getREvpTIn() == null || rec.getREvpPIn() == null) {
			LOG.error(String.format("Record #%d in file %s doesn't contains any information about R_EVP_T_IN or R_EVP_P_IN. It will be discarded", i, fileName));						
			validRecord=false;
		}
		
		if(rec.getREvpTOut() == null || rec.getREvpPOut() == null) {
			LOG.error(String.format("Record #%d in file %s doesn't contains any information about R_EVP_T_OUT or R_EVP_P_OUT. It will be discarded", i, fileName));						
			validRecord=false;
		}
		return validRecord;
	}

}
