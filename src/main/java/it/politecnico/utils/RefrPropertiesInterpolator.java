package it.politecnico.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.log4j.Logger;

import it.politecnico.coolprop.CoolProp;

public class RefrPropertiesInterpolator {
	
	private static Logger LOG = Logger.getLogger(RefrPropertiesInterpolator.class);
	
	static {
		System.loadLibrary("CoolProp");
	}
	
	public static Double getEnthalpyByTemperatureAndPressure(String refrId, double temp, double pressure) {
		try {
			double p = ConverterUtils.barToPascal(pressure);
			double t = ConverterUtils.celsiusToKelvin(temp);
			double h_j_Kj = CoolProp.PropsSI("H", "P", p, "T", t, refrId);
			
			double h_kj_kg = ConverterUtils.unitToKilo(h_j_Kj);
			return truncateMeasure(h_kj_kg);
		} catch (Exception e) {
			LOG.error(e,e);
			return null;
		}
	}
	
	public static Double getTemperatureByEnthalphyAndPressure(String refrId, double enthalphy, double pressure) {
		try {
			double h = ConverterUtils.kiloToUnit(enthalphy);
			double p = ConverterUtils.barToPascal(pressure);
			double k = CoolProp.PropsSI("T", "H", h, "P", p, refrId);
			
			double t = ConverterUtils.kelvinToCelsius(k);
			
			return truncateMeasure(t);
		} catch (Exception e) {
			LOG.error(e,e);
			return null;
		}
	}
	
	public static Double getLiquidSatTempByPressure(String refrId, double pressure) {
		return getSatTempByPressure(refrId, pressure, 0);
	}
	
	public static Double getVapSatTempByPressure(String refrId, double pressure) {
		return getSatTempByPressure(refrId, pressure, 1);
	}

	public static Double getSatTempByPressure(String refrId, double pressure, double status) {
		try {
			double p = ConverterUtils.barToPascal(pressure);
			double K = CoolProp.PropsSI("T", "Q", status, "P", p, "R134a");

			double t = ConverterUtils.kelvinToCelsius(K);
			
			return truncateMeasure(t);
		} catch (Exception e) {
			LOG.error(e,e);
			return null;
		}
	}
	
	private static double truncateMeasure(double measure) {
		return new BigDecimal(measure).setScale(2, RoundingMode.HALF_UP).doubleValue();
	}
	
}
