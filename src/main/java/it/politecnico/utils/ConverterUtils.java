package it.politecnico.utils;

import javax.measure.converter.MultiplyConverter;
import javax.measure.converter.RationalConverter;
import javax.measure.converter.UnitConverter;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

public class ConverterUtils {
	
	private static UnitConverter UNIT_TO_KILO = new RationalConverter(1, 1000);
	private static UnitConverter KILO_TO_UNIT = new MultiplyConverter(1000);
	
	
	public static double barToPascal(double pressure) {
		return Amount.valueOf(pressure, NonSI.BAR).doubleValue(SI.PASCAL);
	}
	
	public static double celsiusToKelvin(double temp) {
		return Amount.valueOf(temp, SI.CELSIUS).doubleValue(SI.KELVIN);
	}
	
	public static double kelvinToCelsius(double temp) {
		return Amount.valueOf(temp, SI.KELVIN).doubleValue(SI.CELSIUS);
	}
	
	public static double unitToKilo(double val) {
		return UNIT_TO_KILO.convert(val);
	}
	
	public static double kiloToUnit(double val) {
		return KILO_TO_UNIT.convert(val);
	}

}
