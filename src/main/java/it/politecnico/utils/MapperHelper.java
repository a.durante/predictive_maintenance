package it.politecnico.utils;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MapperHelper {
	
	private static SqlSessionFactory factory;
	private static String mybatisConfigResourcesPathMap = "mybatis-config.xml";
	
	public static SqlSessionFactory getMapper() throws IOException {
		if (factory == null) {
			synchronized (MapperHelper.class) {
				Reader reader = Resources.getResourceAsReader(mybatisConfigResourcesPathMap);
				String environment = System.getProperty("mybatis-environment");
				SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
				if (environment != null) {
					factory = sqlSessionFactoryBuilder.build(reader, environment);
				} else {
					factory = sqlSessionFactoryBuilder.build(reader);
				}
			}
		}
		return factory;
	}
	
	public static SqlSession openSession() throws IOException {
		return MapperHelper.getMapper().openSession();
	}

}
