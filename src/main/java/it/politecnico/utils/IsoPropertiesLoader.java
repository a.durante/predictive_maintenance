package it.politecnico.utils;

import java.io.IOException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import it.politecnico.mapper.refrisoprop.RefrIsopropertiesMapper;
import it.politecnico.model.refrisoprop.RefrIsoproperties;
import it.politecnico.model.refrisoprop.RefrIsopropertiesExample;

public class IsoPropertiesLoader {
	
	private static Logger			LOG		= Logger.getLogger(IsoPropertiesLoader.class);
	
	private static IsoPropertiesLoader _instance = new IsoPropertiesLoader();
	
	private IsoPropertiesLoader() {}
	
	public static IsoPropertiesLoader getInstance() {
		return _instance;
	}
	
	public List<RefrIsoproperties> loadIsoproperties(String refrId, String sortbyClauses) {
		SqlSession session= null;
		try {
			session = MapperHelper.openSession();
			return this.loadIsoproperties(refrId, sortbyClauses, session);
		} catch (IOException e) {
			LOG.error(e,e);
			throw new IllegalArgumentException(String.format("Unable to get isoproperties info for refrigerant %s", refrId));
		}finally {
			if(session != null) {
				session.close();
			}
		}
	}
	public List<RefrIsoproperties> loadIsoproperties(String refrId, String sortbyClauses, SqlSession session) {
		RefrIsopropertiesMapper mapper = session.getMapper(RefrIsopropertiesMapper.class);
		
		RefrIsopropertiesExample example = new RefrIsopropertiesExample();
		example.createCriteria().andRefrigerantIdEqualTo(refrId);
		
		example.setOrderByClause(sortbyClauses);
		
		List<RefrIsoproperties> prop = mapper.selectByExample(example);
		if(prop == null) {
			throw new IllegalArgumentException(String.format("Database does not cotains any isoproperties referred to %s refrigerant", refrId));
		}
		return prop;
	}
	

}
