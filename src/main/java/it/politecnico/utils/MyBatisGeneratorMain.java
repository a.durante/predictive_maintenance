package it.politecnico.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

/**
 * 
 * @author f.raimondi Classe contenente il main per il Code Generator di
 * MyBatis.
 * 
 */
public class MyBatisGeneratorMain {

	/**
	 * Necessita un arguments tipo: ${workspace_loc}/OBT_DataAccess
	 * /myBatisGenerator/myBatisConfig/ibatisConfig-Staging.xml
	 */
	
	private static Logger			LOG		= Logger.getLogger(MyBatisGeneratorMain.class);
//
	private static final String[]	conf_all	= {
			"src/main/resources/com/reply/obt/be/dao/mybatis/generator/configfile/mybatisConf-Planning.xml" 	
	};
//
	public static void main(String[] args) throws Exception {
		List<String> warnings = new ArrayList<String>();
		boolean overwrite = true;
		if (args.length == 0) {
			LOG.info("### ERRORE generazione codice Mapper. Argument non specificato!!!");
			System.out.println("### ERRORE generazione codice Mapper. Argument non specificato!!!");
			return;
		}
		if (!args[0].equals("ALL")) {
			LOG.info("#### Lancio generazione codice da: " + args[0]);
			System.out.println("#### Lancio generazione codice da: " + args[0]);

			File configFile = new File(args[0]);
			ConfigurationParser cp = new ConfigurationParser(warnings);
			Configuration config = cp.parseConfiguration(configFile);
			DefaultShellCallback callback = new DefaultShellCallback(overwrite);

			MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
			myBatisGenerator.generate(null);
			LOG.info("	### Warning generazione Mapper:");
			System.out.println("	### Warning generazione Mapper:");

		} else {
			for (String file : conf_all) {
				LOG.info("#### Lancio generazione codice da: " + file);
				System.out.println("#### Lancio generazione codice da: " + file);

				File configFile = new File(System.getProperty("user.dir") + File.separator + file);
				ConfigurationParser cp = new ConfigurationParser(warnings);
				Configuration config = cp.parseConfiguration(configFile);
				DefaultShellCallback callback = new DefaultShellCallback(overwrite);

				MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
				myBatisGenerator.generate(null);
			}

		}
		if (warnings.size() == 0) {
			LOG.info("nessuno!");
			System.out.println("nessuno!");
		} else {
			for (String warn : warnings) {
				LOG.info("	-	" + warn);
				System.out.println("	-	" + warn);
			}
		}
		LOG.info("### FINE generazione codice Mapper.");
		System.out.println("### FINE generazione codice Mapper.");
	}
}
